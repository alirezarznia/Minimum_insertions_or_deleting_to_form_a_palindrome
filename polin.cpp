#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(x, MP(y,z))
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long ,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;
using namespace std;
string str;
vector<vector<ll> > dp;

ll F(ll l , ll h)
{
    if(l >= h) return 0;
    ll &ret = dp[l][h];
    if(ret != -1)   return ret;
    return ret = (str[l] == str[h]) ? F(l+1 , h-1) : min(F(l+1 , h) , F(l ,h-1))+1;
}
int main(){
 //  Test;
    ll t;
    cin>>t;
    while(t--){
        cin>>str;
        ll qq =str.size();
         dp.assign(qq+1 , vector<ll>(qq+1 , -1));
        cout<< F(0 , str.size()-1) <<endl;
    }
}
// n- F(0 , str.size()-1) is longest sequence of polindrome